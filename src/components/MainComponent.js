import React, { Component } from 'react';
import {  Redirect, Route, Switch } from 'react-router-dom'

import Menu from './MenuComponent';
import Home from './HomeComponent'
import Header from './HeaderComponent'
import Footer from './FooterComponent'

import { DISHES } from '../shared/dishes';  




class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
        dishes: DISHES,
        selectedDish: null
    };
  }

  onDishSelect(dishId) {
    this.setState({ selectedDish: dishId});
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
            <Route path='/home' component={Home} />
            <Route exact path='/menu' component={() => <Menu dishes={this.state.dishes} onClick={(dishId) => this.onDishSelect(dishId)} /> } />
            <Redirect to='/home' />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default Main;