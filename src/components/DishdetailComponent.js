import React, { Fragment } from 'react'
import { Card, CardImg, CardBody, CardTitle, CardText } from 'reactstrap'

function RenderDish(dish){
    return(
        <Card>
            <CardImg width="100%" src={dish.image} alt={dish.name} />
            <CardBody>
                <CardTitle> {dish.name} </CardTitle>
                <CardText> {dish.description} </CardText>
            </CardBody>
        </Card>
    )
}

function RenderComments(comments){

    return(
        <>
            { comments === null ? <div></div> :
                <div>
                    <h4>Comments</h4>
                        <ul className="list-unstyled">
                            {
                                comments.map(comment => {
                                    return (
                                        <li key={comment.id}>
                                            <p>{comment.comment}</p>
                                            <p> -- { comment.author }, { new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))} </p>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                </div>
            }
        </>
    )
}

const DishDetail = (props) => {

    return(
        <div className="container">
                 <div className="row">
                 { !props.dish ? <div></div> : 
                     (
                         <>
                             <div className="col-12 col-md-5 m-1">
                                 {RenderDish(props.dish)}
                             </div>
                             <div className="col-12 col-md-5 m-1">
                                 {
                                     RenderComments(props.dish.comments)
                                 }
                             </div>
                         </>
                     )
                 } 
                 </div>
             </div>
    )
}

export default DishDetail

// export default class DishDetail extends React.Component{

//     // renderDish(dish){

//     //     return(
//     //         <Card>
//     //             <CardImg width="100%" src={dish.image} alt={dish.name} />
//     //             <CardBody>
//     //                 <CardTitle> {dish.name} </CardTitle>
//     //                 <CardText> {dish.description} </CardText>
//     //             </CardBody>
//     //         </Card>
//     //     )
//     // }

//     renderComments(comments){

//         // return(
//         //     <>
//         //         { comments === null ? <div></div> :
//         //             <div>
//         //                 <h4>Comments</h4>
//         //                     <ul className="list-unstyled">
//         //                         {
//         //                             comments.map(comment => {
//         //                                 return (
//         //                                     <li key={comment.id}>
//         //                                         <p>{comment.comment}</p>
//         //                                         <p> -- { comment.author }, { new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))} </p>
//         //                                     </li>
//         //                                 )
//         //                             })
//         //                         }
//         //                     </ul>
//         //             </div>
//         //         }
//         //     </>
//         // )
//     }

//     render(){

//         return(
//             <div className="container">
//                 <div className="row">
//                 { !this.props.dish ? <div></div> : 
//                     (
//                         <>
//                             <div className="col-12 col-md-5 m-1">
//                                 {this.renderDish(this.props.dish)}
//                             </div>
//                             <div className="col-12 col-md-5 m-1">
//                                 {
//                                     this.renderComments(this.props.dish.comments)
//                                 }
//                             </div>
//                         </>
//                     )
//                 } 
//                 </div>
//             </div>
            
//         )
//     }
// }