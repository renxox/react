import React from 'react'
import { BrowserRouter } from 'react-router-dom'

import Main from './components/MainComponent';

import './App.css'

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Main />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
